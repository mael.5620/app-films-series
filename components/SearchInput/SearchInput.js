import React, {useState} from 'react';
import { TextField, Select, MenuItem } from '@material-ui/core'
import ApiConnect from '../../services/ApiConnect'
import styles from './SearchInput.module.css'

const SearchInput = ({setResults, setSearchQuery}) => {
    const [query, setQuery] = useState("");
    const [type, setType] = useState("movie");

    const handleSubmit = (e) => {
        if (e.key === 'Enter' && query != "") {
            e.preventDefault();
            if (type === 'movie') {
                ApiConnect.searchForMovie(query)
                .then(res => (setResults({
                    total_results: res.total_results,
                    results: res.results
                }), setSearchQuery(query)))
            } else if (type === 'tvShow') {
                ApiConnect.searchForTvShow(query)
                .then(res => (setResults({
                    total_results: res.total_results,
                    results: res.results
                }), setSearchQuery(query)))
            }
        }
    }

    return (
        <div className={styles.container} >
            <TextField 
                onChange={e => setQuery(e.target.value)}
                onKeyPress={e => handleSubmit(e)}
                className={styles.text_input}
                placeholder="Recherchez un film ou une série ..."
            />
            <Select 
                onChange={e => setType(e.target.value)}
                defaultValue='movie'
                className={styles.select}
            >
                <MenuItem value="movie" >Film</MenuItem>
                <MenuItem value="tvShow" >Série</MenuItem>
            </Select>
        </div>
    );
};

export default SearchInput;