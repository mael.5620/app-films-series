import React, { Component } from 'react';
import Link from 'next/link'
import ApiConnect from '../../services/ApiConnect'
import styles from './TrendingTvShows.module.css'

class TrendingTvShows extends Component {
    constructor(props) {
        super(props);

        this.state = {tvShows: []}
    }

    componentDidMount() {
        ApiConnect.getPopularTvShows()
        .then(res => this.setState({tvShows: res.results}))
    }
    
    render() {
        return (
            <div className={styles.global_container} >
                <h2>Séries populaires</h2>
                <div className={styles.grid_container} >
                    {
                        this.state.tvShows.map(item => (
                            <div>
                                <p className={styles.title} > {item.name} </p>
                                {
                                    item.poster_path ? (
                                        <Link href={`/tv/${item.id}`} >
                                            <a><img className={styles.poster} src={`https://image.tmdb.org/t/p/w185${item.poster_path}`} /></a>
                                        </Link>
                                        ) : (
                                        <div className={styles.empty_image}>
                                            <div className={styles.icon}></div>
                                        </div>
                                    )
                                }
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default TrendingTvShows;
