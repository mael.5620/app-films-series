import React from 'react';
import Link from 'next/link'
import styles from './SearchResults.module.css'

const SearchResults = ({searchRes, searchQuery}) => {
    return (
        <div className={styles.global_container} >
            <h2>Résultats de la recherche pour : {searchQuery} </h2>
            
            <div className={styles.results_container} >
                {
                    searchRes.results.map(item => (
                        <div>
                            {
                                item.title ? (
                                    <>
                                        <p className={styles.title} > {item.title} </p>
                                        {
                                            item.poster_path ? (
                                                <Link href={`/movie/${item.id}`} >
                                                    <a><img className={styles.poster} src={`https://image.tmdb.org/t/p/w185${item.poster_path}`} /></a>
                                                </Link>
                                                ) : (
                                                <div className={styles.empty_image}>
                                                    <div className={styles.icon}></div>
                                                </div>
                                            )
                                        }
                                    </>
                                ) : (
                                    <>
                                        <p className={styles.title}> {item.original_name} </p>
                                        {
                                            item.poster_path ? (
                                                <Link href={`/tv/${item.id}`} >
                                                    <a><img className={styles.poster} src={`https://image.tmdb.org/t/p/w185${item.poster_path}`} /></a>
                                                </Link>
                                                ) : (
                                                <div className={styles.empty_image}>
                                                    <div className={styles.icon}></div>
                                                </div>
                                            )
                                        }
                                    </>
                                )
                            }
                        </div>
                    ))
                }
            </div>
        </div>
    );
};

export default SearchResults;