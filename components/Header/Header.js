import React from 'react';
import Link from 'next/link'
import styles from './Header.module.css'

const Header = () => {
    return (
        <div className={styles.header} >
            <Link href='/'>
                <a className={styles.link} >Accueil</a>
            </Link>
            <Link href='/popular-movies'>
                <a className={styles.link} >Films populaires</a>
            </Link>
            <Link href='/popular-tv'>
                <a className={styles.link} >Séries populaires</a>
            </Link>
        </div>
    );
};

export default Header;