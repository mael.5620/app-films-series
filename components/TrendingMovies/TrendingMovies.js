import React, { Component } from 'react';
import Link from 'next/link'
import ApiConnect from '../../services/ApiConnect'
import styles from './TrendingMovies.module.css'


class TrendingMovies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movies : []
        }
        
    }
    

    componentDidMount() {
        ApiConnect.getPopularMovies()
        .then(res => this.setState({movies: res.results}))
    }

    render() {
        return (
            <div className={styles.global_container} >
                <h2>Films populaires</h2>
                <div className={styles.grid_container} >
                    {
                        this.state.movies.map(item => (
                            <div>
                                <p className={styles.title} > {item.original_title} </p>
                                {
                                    item.poster_path ? (
                                        <Link href={`/movie/${item.id}`} >
                                            <a><img className={styles.poster} src={`https://image.tmdb.org/t/p/w185${item.poster_path}`} /></a>
                                        </Link>
                                        ) : (
                                        <div className={styles.empty_image}>
                                            <div className={styles.icon}></div>
                                        </div>
                                    )
                                }
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }
}

export default TrendingMovies;