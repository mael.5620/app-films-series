import React from 'react';
import styles from './movieCard.module.css'

const MovieCard = ({movie}) => {
    console.log(movie.poster_path)
    return (
        <div>
            <p className={styles.title} > {movie.title} </p>
            <img src={`https://image.tmdb.org/t/p/w185${movie.poster_path}`} alt=""/>
        </div>
    );
};

export default MovieCard;

