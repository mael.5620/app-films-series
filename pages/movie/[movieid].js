import React, { Component } from 'react';
import Header from '../../components/Header/Header'
import ApiConnect from '../../services/ApiConnect'
import styles from '../../styles/Movie.module.css'



class Movie extends Component {

    constructor(props) {
        super(props);
        
        this.state = {}
    }
    
    static getInitialProps({query}) {
        return { query }
    }

    componentDidMount() {
        const movieId = this.props.query.movieid
        ApiConnect.getMovieById(movieId)
        .then(res => this.setState({...res}))
    }
    
    render() {
        const { original_title, overview, release_date, poster_path } = this.state
        return (
            <div className={styles.container} >
                <Header />
                <h1 className={styles.title} > {original_title} </h1>
                <img className={styles.poster} src={`https://image.tmdb.org/t/p/w342${poster_path}`} alt=""/>
                <p className={styles.release_date} > {release_date} </p>
                <p className={styles.overview} > {overview} </p>
                
            </div>
        );
    }
}

export default Movie;


