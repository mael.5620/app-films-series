import React from 'react';
import Header from '../components/Header/Header';
import TrendingMovies from '../components/TrendingMovies/TrendingMovies';

const movies = () => {
    return (
        <div>
            <Header/>
        
            <TrendingMovies />
        </div>
    );
};

export default movies;