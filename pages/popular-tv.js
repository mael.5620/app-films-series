import React from 'react';
import Header from '../components/Header/Header';
import TrendingTvShows from '../components/TrendingTvShows/TrendingTvShows'

const tv = () => {
    return (
        <div>
            <Header/>
        
            <TrendingTvShows />
        </div>
    );
};

export default tv;