import styles from '../styles/Home.module.css'
import Header from '../components/Header/Header'
import SearchInput from '../components/SearchInput/SearchInput'
import SearchResults from '../components/SearchResults/SearchResults'
import { useState } from 'react'

const Home = () => {
  const [searchRes, setSearchRes] = useState({});
  const [searchQuery, setSearchQuery] = useState("");
  
  return (
    <div className={styles.container}>
      <Header />

      <h1 className={styles.title}> Découvrez des films et des séries </h1>
      <h2 className={styles.subtitle}> Cette application est basée sur l'API <i>The Movie Database</i></h2>

      <SearchInput setResults={setSearchRes} setSearchQuery={setSearchQuery} />
      {
        searchRes.total_results > 0 ? (
          <SearchResults searchRes={searchRes} searchQuery={searchQuery} />
        ) : searchRes.total_results === 0 ? (
          <h3 className={styles.no_results} >Aucun résultat</h3>
        ) : null
      }
    </div>
  )
}

export default Home