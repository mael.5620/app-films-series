import React, { Component } from 'react';
import Header from '../../components/Header/Header'
import ApiConnect from '../../services/ApiConnect'
import styles from '../../styles/TvShow.module.css'



class TvShow extends Component {

    constructor(props) {
        super(props);
        
        this.state = {}
    }
    
    static getInitialProps({query}) {
        return { query }
    }

    componentDidMount() {
        const tvid = this.props.query.tvid
        ApiConnect.getTvShowById(tvid)
        .then(res => this.setState({...res}))
    }
    
    render() {
        const { name, overview, number_of_seasons, number_of_episodes, poster_path } = this.state
        return (
            <div className={styles.container} >
                <Header />
                <h1 className={styles.title} > {name} </h1>
                <img className={styles.poster} src={`https://image.tmdb.org/t/p/w342${poster_path}`} alt=""/>
                <p className={styles.number_of_seasons} > {number_of_seasons} saison(s) </p>
                <p className={styles.number_of_episodes} > {number_of_episodes} épisode(s) </p>
                <p className={styles.overview} > {overview} </p>
                
            </div>
        );
    }
}

export default TvShow;


