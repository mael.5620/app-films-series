import axios from 'axios'

export const baseUrl = "https://api.themoviedb.org"

export default {
    getPopularMovies: () => {
        return axios
            .get(`${baseUrl}/3/discover/movie`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    },  
    getPopularTvShows: () => {
        return axios
            .get(`${baseUrl}/3/discover/tv`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    },  
    searchForMovie: (query) => {
        return axios
            .get(`${baseUrl}/3/search/movie`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                    query: query
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    },
    searchForTvShow: (query) => {
        return axios
            .get(`${baseUrl}/3/search/tv`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                    query: query
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    },
    getMovieById: (id) => {
        return axios
            .get(`${baseUrl}/3/movie/${id}`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                    movie_id: id
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    },
    getTvShowById: (id) => {
        return axios
            .get(`${baseUrl}/3/tv/${id}`, {
                params : {
                    api_key: process.env.NEXT_PUBLIC_API_KEY,
                    tv_id: id
                }
            })
            .then(res => res.data)
            .catch(error => console.log(error))
    }
    


}